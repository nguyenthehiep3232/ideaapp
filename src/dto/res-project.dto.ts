import { Exclude, Expose, Type } from "class-transformer";
import { IsDate, IsInt, IsNotEmpty, IsString, Length } from "class-validator";

@Exclude()
export class ResProjectDto {
  @Expose()
  @IsString()
  name: string;

  @Expose()
  @IsString()
  category: string;

  @Expose()
  @IsInt()
  projectedSpend: number;

  @Expose()
  @IsInt()
  projectedVariance: number; 

  @Expose()
  @IsDate()
  @Type(() => Date)
  projectStartedAt: Date;

  @Expose()
  @IsDate()
  @Type(() => Date)
  projectEndedAt: Date;

}
