import { Type } from "class-transformer";
import { IsDate, IsInt, IsNotEmpty, IsString, Length } from "class-validator";

export class ReqProjectDto {
  @Length(1,255)
  @IsString()
  name: string;

  @IsString()
  category: string;

  @IsInt()
  projectedSpend: number;

  @IsInt()
  projectedVariance: number; 

  @IsDate()
  @Type(() => Date)
  projectStartedAt: Date;

  @IsDate()
  @Type(() => Date)
  projectEndedAt: Date;
}
