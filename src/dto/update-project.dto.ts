import { Transform, Type } from "class-transformer";
import { IsDate, IsInt, IsString, Length } from "class-validator";
import * as moment from 'moment';
import { Moment } from 'moment';

export class UpdateProject {
  @IsString()
  @Length(1,255)
  name?: string;

  @IsString()
  categoty?: string;

  @IsInt()
  projectedSpend?: number;

  @IsInt()
  projectedVariance?: number;

  @Type(() => Date)
  @Transform(({value}): Date => new Date(value))
  projectStartedAt: Moment;

  @IsDate()
  @Type(() => Date)
  @Transform(({value}): Date => new Date(value))
  projectEndedAt?: Date;
}
