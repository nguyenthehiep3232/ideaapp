import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity({
  name: 'project'
})
export class Project {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'varchar',
    length: 255,
    name: 'name'
  })
  name: string;

  @Column({
    type: 'varchar',
    name: 'category',
    length: 255,
    default: 'client'
  })
  category: string;

  @Column({
    name: 'projected_spend',
    default: 0
  })
  projectedSpend: number;

  @Column({
    name: 'projected_variance',
    default: 0
  })
  projectedVariance: number;

  @Column({
    name: 'revenue_recognised',
    default: 0
  })
  revenueRecognised: number;

  @Column({
    type: 'datetime',
    name: 'project_started_at'
  })
  projectStartedAt: Date;

  @Column({
    type: 'datetime',
    name: 'project_ended_at',
    nullable: true
  })
  projectEndedAt: Date;

  @CreateDateColumn({
    type: 'datetime',
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP'
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'datetime',
    name: 'updated_at',
    default: () => 'CURRENT_TIMESTAMP'
  })
  updatedAt: Date;

  @DeleteDateColumn({
    name: 'deleted_at',
    nullable: true
  })
  deletedAt: Date;
}
