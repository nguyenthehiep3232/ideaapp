import { Injectable, NotFoundException, Param, Req } from '@nestjs/common';
import { UpdateProject } from 'src/dto/update-project.dto';
import { Project } from 'src/entities/project.entity';
import { ProjectRepository } from 'src/repositories/project.repository';

@Injectable()
export class ProjectService {
  constructor( private projectRepository: ProjectRepository) {};
  private projects: Array<Project> = [];
  
  async createProject(project: Project): Promise<Project> {
    const newProject = await this.projectRepository.create(project);
    this.projects.push(newProject);
    return this.projectRepository.save(newProject);
  }

  async getAllProject(): Promise<Project[]> {
    return await this.projectRepository.find();
  }

  async getProjectById(id: number): Promise<Project> {
    const project: Project = await this.projects.find(project => project.id === id);  ;
    if (!project) {
      throw new NotFoundException('Project not found');
    }
    return project;
  }

  async updateProject(id: number, projectDto : UpdateProject): Promise<Project> {
    const project = await this.getProjectById(id);
    if(!project) {
      throw new NotFoundException('Project not found');
    }
    const projectUpdate = {...project, ...projectDto };

    return await this.projectRepository.save(projectUpdate);
  }

  async deleteProject(id: number): Promise<string> {
    const projectDeleted = await this.projectRepository.softDelete(id);
    if(!projectDeleted.affected){
      throw new NotFoundException('Project not found')
    }
    return "Project has been deleted";
  }
}
