import { Body, Get, Controller, Post, Put, Patch, Delete, Param, ParseIntPipe } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { ReqProjectDto } from 'src/dto/req-project.dto';
import { ResProjectDto } from 'src/dto/res-project.dto';
import { UpdateProject } from 'src/dto/update-project.dto';
import { Project } from 'src/entities/project.entity';
import { ProjectService } from './project.service';

@Controller('project')
export class ProjectController {
  constructor(private projectService: ProjectService) {}

  @Post('/create')
  async createProject(@Body() body: ReqProjectDto): Promise<ResProjectDto> {
    const transformProject = plainToClass(Project, body);
    const newProject: Project = await this.projectService.createProject(transformProject);
    return plainToClass(ResProjectDto, newProject);
  }

  @Get()
  async getAll(): Promise<Project[]> {
    return await this.projectService.getAllProject();
  }

  @Get('/:id')
  async getOneById(@Param('id', ParseIntPipe) id: number): Promise<Project> {
    const project = await this.projectService.getProjectById(id);
    return project;
  }

  @Patch('/:id/update')
  async updateProject(@Param('id', ParseIntPipe) id: number, @Body() dto: UpdateProject): Promise<Project> {
    const projectUpdate = await this.projectService.updateProject(id, dto);
    return projectUpdate;
  }

  @Delete('/:id/delete')
  async deleteProject(@Param('id', ParseIntPipe) id: number): Promise<String> {
    return await this.projectService.deleteProject(id);
  }
}
