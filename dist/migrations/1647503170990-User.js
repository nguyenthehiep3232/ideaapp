"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.User1647503170990 = void 0;
class User1647503170990 {
    constructor() {
        this.name = 'User1647503170990';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE \`users\` (\`id\` int NOT NULL AUTO_INCREMENT, \`first_name\` varchar(255) NOT NULL, \`last_name\` varchar(255) NOT NULL, \`is_active\` tinyint NOT NULL DEFAULT '1', \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`project\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`category\` varchar(255) NOT NULL DEFAULT 'client', \`projected_spend\` int NOT NULL DEFAULT '0', \`projected_variance\` int NOT NULL DEFAULT '0', \`projected_recognised\` int NOT NULL DEFAULT '0', \`project_started_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`project_ended_at\` datetime(6) NULL DEFAULT CURRENT_TIMESTAMP(6), \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deleted_at\` datetime(6) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE \`project\``);
        await queryRunner.query(`DROP TABLE \`users\``);
    }
}
exports.User1647503170990 = User1647503170990;
//# sourceMappingURL=1647503170990-User.js.map