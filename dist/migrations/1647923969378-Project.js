"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Project1647923969378 = void 0;
class Project1647923969378 {
    constructor() {
        this.name = 'Project1647923969378';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`project\` DROP COLUMN \`project_started_at\``);
        await queryRunner.query(`ALTER TABLE \`project\` ADD \`project_started_at\` timestamp NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`project\` DROP COLUMN \`project_ended_at\``);
        await queryRunner.query(`ALTER TABLE \`project\` ADD \`project_ended_at\` timestamp NULL`);
        await queryRunner.query(`ALTER TABLE \`project\` DROP COLUMN \`created_at\``);
        await queryRunner.query(`ALTER TABLE \`project\` ADD \`created_at\` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP`);
        await queryRunner.query(`ALTER TABLE \`project\` DROP COLUMN \`updated_at\``);
        await queryRunner.query(`ALTER TABLE \`project\` ADD \`updated_at\` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`project\` DROP COLUMN \`updated_at\``);
        await queryRunner.query(`ALTER TABLE \`project\` ADD \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`project\` DROP COLUMN \`created_at\``);
        await queryRunner.query(`ALTER TABLE \`project\` ADD \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`project\` DROP COLUMN \`project_ended_at\``);
        await queryRunner.query(`ALTER TABLE \`project\` ADD \`project_ended_at\` datetime(6) NULL DEFAULT CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`project\` DROP COLUMN \`project_started_at\``);
        await queryRunner.query(`ALTER TABLE \`project\` ADD \`project_started_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`);
    }
}
exports.Project1647923969378 = Project1647923969378;
//# sourceMappingURL=1647923969378-Project.js.map