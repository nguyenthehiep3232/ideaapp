"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Project1647965905505 = void 0;
class Project1647965905505 {
    constructor() {
        this.name = 'Project1647965905505';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE \`project\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`category\` varchar(255) NOT NULL DEFAULT 'client', \`projected_spend\` int NOT NULL DEFAULT '0', \`projected_variance\` int NOT NULL DEFAULT '0', \`revenue_recognised\` int NOT NULL DEFAULT '0', \`project_started_at\` timestamp NOT NULL, \`project_ended_at\` timestamp NULL, \`created_at\` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, \`updated_at\` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, \`deleted_at\` datetime(6) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE \`project\``);
    }
}
exports.Project1647965905505 = Project1647965905505;
//# sourceMappingURL=1647965905505-Project.js.map