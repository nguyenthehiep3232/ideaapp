export declare class Project {
    id: number;
    name: string;
    category: string;
    projectedSpend: number;
    projectedVariance: number;
    revenueRecognised: number;
    projectStartedAt: Date;
    projectEndedAt: Date;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
}
