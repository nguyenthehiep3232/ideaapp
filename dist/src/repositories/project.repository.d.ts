import { Project } from 'src/entities/project.entity';
import { Repository } from 'typeorm';
export declare class ProjectRepository extends Repository<Project> {
}
