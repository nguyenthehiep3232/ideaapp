import { User } from 'src/entities/user.entity';
import { UserRepository } from 'src/repositories/user.repository';
export declare class UserService {
    private readonly userRepository;
    constructor(userRepository: UserRepository);
    getUserById(userId: number): Promise<User>;
}
