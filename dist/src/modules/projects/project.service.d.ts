import { ProjectDto } from 'src/dto/project.dto';
import { ProjectRepository } from 'src/repositories/project.repository';
import { Project } from '../../entities/project.entity';
export declare class ProjectService {
    private projectRepository;
    constructor(projectRepository: ProjectRepository);
    createProject(body: ProjectDto): Promise<Project>;
}
