import { ProjectDto } from 'src/dto/project.dto';
import { ProjectService } from './project.service';
import { Project } from '../../entities/project.entity';
export declare class ProjectController {
    private projectService;
    constructor(projectService: ProjectService);
    createProject(createProjectDto: ProjectDto, res: Response): Promise<Project>;
}
