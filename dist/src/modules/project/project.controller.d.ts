import { ReqProjectDto } from 'src/dto/req-project.dto';
import { ResProjectDto } from 'src/dto/res-project.dto';
import { ProjectService } from './project.service';
export declare class ProjectController {
    private projectService;
    constructor(projectService: ProjectService);
    createProject(body: ReqProjectDto): Promise<ResProjectDto>;
}
