import { Project } from 'src/entities/project.entity';
import { ProjectRepository } from 'src/repositories/project.repository';
export declare class ProjectService {
    private projectRepository;
    constructor(projectRepository: ProjectRepository);
    createProject(project: Project): Promise<Project>;
}
