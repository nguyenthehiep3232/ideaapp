export declare class ProjectDto {
    name: string;
    category: string;
    projected_spend: number;
    projected_variance: number;
    project_started_at: Date;
    project_ended_at: Date;
}
