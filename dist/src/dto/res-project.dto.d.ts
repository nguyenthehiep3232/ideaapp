export declare class ResProjectDto {
    name: string;
    category: string;
    projectedSpend: number;
    projectedVariance: number;
    projectStartedAt: Date;
    projectEndedAt: Date;
}
