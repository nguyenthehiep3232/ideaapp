export declare class ReqProjectDto {
    name: string;
    category: string;
    projectedSpend: number;
    projectedVariance: number;
    projectStartedAt: Date;
    projectEndedAt: Date;
}
