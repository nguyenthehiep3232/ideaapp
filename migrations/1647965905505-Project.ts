import {
  MigrationInterface,
  QueryRunner
} from "typeorm";

export class Project1647965905505 implements MigrationInterface {
    name = 'Project1647965905505'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`project\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`category\` varchar(255) NOT NULL DEFAULT 'client', \`projected_spend\` int NOT NULL DEFAULT '0', \`projected_variance\` int NOT NULL DEFAULT '0', \`revenue_recognised\` int NOT NULL DEFAULT '0', \`project_started_at\` timestamp NOT NULL, \`project_ended_at\` timestamp NULL, \`created_at\` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, \`updated_at\` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, \`deleted_at\` datetime(6) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE \`project\``);
    }

}
